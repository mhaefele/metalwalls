! Module to compute long-range part of electrostatic (Coulomb) potential
module MW_coulomb_lr_pim
   use MW_kinds, only: wp
   use MW_ewald, only: MW_ewald_t
   use MW_box, only: MW_box_t
   use MW_constants, only: twopi, block_vector_size
   use MW_electrode, only: MW_electrode_t
   use MW_molecule, only: MW_molecule_t
   use MW_ion, only: MW_ion_t
   use MW_localwork, only: MW_localwork_t
   implicit none
   private

   ! Public subroutines
   ! ------------------
   public :: gradQelec_electricfield !grad_Q [grad_mu [U^lr_muQ]]
   public :: gradmumelt_electricfield !grad_mu [grad_mu [U^lr_mumu]]
   public :: mumelt2Qelec_potential !grad_Q [U^lr_muQ]
   public :: qmelt2mumelt_electricfield !-grad_mu [U^lr_qmu]
   public :: mumelt2mumelt_electricfield !-grad_mu [U^lr_mumu]
   public :: Qelec2mumelt_electricfield !-grad_mu [U^lr_muQ]
   public :: melt_forces !-grad_r [(U^lr_qq + U^lr_qmu + U^lr_mumu) + (U^lr_QQ) + (U^lr_qQ + U^lr_muQ)]
   public :: energy !(U^lr_qq + U^lr_qmu + U^lr_mumu) + (U^lr_QQ) + (U^lr_qQ + U^lr_muQ)

contains

   subroutine gradQelec_electricfield(localwork, ewald, box, electrodes, &
      gradQ_efield)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork       !< Local work assignment
      type(MW_box_t),       intent(in)    :: box             !< Simulation box parameters
      type(MW_electrode_t), intent(in)    :: electrodes(:)   !< electrodes parameters

      ! Parameters out
      ! --------------
      real(wp),             intent(inout) :: gradQ_efield(:,:)      !< Electric field on ions
      type(MW_ewald_t),     intent(inout) :: ewald           !< Ewald summation parameters

      ! Local
      ! -----
      integer :: num_ions, num_elec_types
      integer :: i, itype, j, jx, jy, jz
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky
      real(wp) :: cos_kxkykz_i, sin_kxkykz_i, cos_kxkykz_j, sin_kxkykz_j
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ions = size(gradQ_efield,1)/3
      num_elec_types = size(electrodes,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset

      volfactor = 2.0_wp/box%area(3)

      do itype = 1, num_elec_types
         num_blocks = 0
         if (electrodes(itype)%count > 0) then
            num_blocks = (electrodes(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = electrodes(itype)%offset + electrodes(itype)%count

         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + 1 + electrodes(itype)%offset
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  ! Atoms contribution to structure factor
                  Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)

                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_elec(i,l)
                     sin_kx = ewald%sin_kx_elec(i,l)
                     cos_ky = ewald%cos_ky_elec(i,mabs)
                     sin_ky = ewald%sin_ky_elec(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_elec(i,nabs)
                     sin_kz = ewald%sin_kz_elec(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz_i = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz_i = sin_kxky*cos_kz + cos_kxky*sin_kz

                     do j = 1, num_ions

                        jx = j
                        jy = j + num_ions
                        jz = j + num_ions*2

                        cos_kx = ewald%cos_kx_ions(j,l)
                        sin_kx = ewald%sin_kx_ions(j,l)
                        cos_ky = ewald%cos_ky_ions(j,mabs)
                        sin_ky = ewald%sin_ky_ions(j,mabs)*sign_m
                        cos_kz = ewald%cos_kz_ions(j,nabs)
                        sin_kz = ewald%sin_kz_ions(j,nabs)*sign_n

                        cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                        sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                        cos_kxkykz_j = cos_kxky*cos_kz - sin_kxky*sin_kz
                        sin_kxkykz_j = sin_kxky*cos_kz + cos_kxky*sin_kz

                        gradQ_efield(jx,i) = gradQ_efield(jx,i) + Sk_alpha* &
                              (sin_kxkykz_j*cos_kxkykz_i - cos_kxkykz_i*sin_kxkykz_j)*kx
                        gradQ_efield(jy,i) = gradQ_efield(jy,i) + Sk_alpha* &
                              (sin_kxkykz_j*cos_kxkykz_i - cos_kxkykz_i*sin_kxkykz_j)*ky
                        gradQ_efield(jz,i) = gradQ_efield(jz,i) + Sk_alpha* &
                              (sin_kxkykz_j*cos_kxkykz_i - cos_kxkykz_i*sin_kxkykz_j)*kz

                     end do
                  end do
               end if
            end do ! imode
         end do
      end do

   end subroutine gradQelec_electricfield

   !================================================================================
   ! Compute the gradient of the electric field for each ion in the melt with respect
   ! to variations of other dipoles in the melt
   subroutine gradmumelt_electricfield(localwork, ewald, box, ions, &
         gradmu_efield)
      implicit none

      ! Parameters Input
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork       !< Local work assignment
      type(MW_box_t),       intent(in)    :: box             !< Simulation box parameters
      type(MW_ion_t),       intent(in)    :: ions(:)         !< ions parameters

      ! Parameters Input/Output
      ! --------------
      type(MW_ewald_t),     intent(inout) :: ewald              !< Ewald summation parameters
      real(wp),             intent(inout) :: gradmu_efield(:,:) !< matrix containing the
      !< gradient of the field

      ! Local
      ! -----
      integer :: num_ion_types, num_ions
      integer :: i, itype, j, ix, iy, iz, jx, jy, jz
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz, cos_kxky, sin_kxky
      real(wp) :: cos_kxkykz_i, sin_kxkykz_i, cos_kxkykz_j, sin_kxkykz_j
      real(wp) :: coskicoskj_p_sinkisinkj
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ion_types = size(ions,1)
      num_ions = size(gradmu_efield,1)/3

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes executed globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank
      volfactor = 2.0_wp/box%area(3)

      ! Setup cache blocking parameters
      do itype = 1, num_ion_types

         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)

                  do i = istart_block, iend_block

                     ix = i
                     iy = i + num_ions
                     iz = i + num_ions*2

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz_i = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz_i = sin_kxky*cos_kz + cos_kxky*sin_kz

                     !Non-parallelized loop over all particles (not ready for non-polarizable species)
                     do j=1,i

                        jx = j
                        jy = j + num_ions
                        jz = j + num_ions*2

                        cos_kx = ewald%cos_kx_ions(j,l)
                        sin_kx = ewald%sin_kx_ions(j,l)
                        cos_ky = ewald%cos_ky_ions(j,mabs)
                        sin_ky = ewald%sin_ky_ions(j,mabs)*sign_m
                        cos_kz = ewald%cos_kz_ions(j,nabs)
                        sin_kz = ewald%sin_kz_ions(j,nabs)*sign_n

                        cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                        sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                        cos_kxkykz_j = cos_kxky*cos_kz - sin_kxky*sin_kz
                        sin_kxkykz_j = sin_kxky*cos_kz + cos_kxky*sin_kz

                        coskicoskj_p_sinkisinkj = cos_kxkykz_i*cos_kxkykz_j + sin_kxkykz_i*sin_kxkykz_j

                        gradmu_efield(jx,ix) = gradmu_efield(jx,ix) + &
                              Sk_alpha*coskicoskj_p_sinkisinkj*kx*kx !xx
                        gradmu_efield(jy,ix) = gradmu_efield(jy,ix) + &
                              Sk_alpha*coskicoskj_p_sinkisinkj*kx*ky !yx
                        gradmu_efield(jz,ix) = gradmu_efield(jz,ix) + &
                              Sk_alpha*coskicoskj_p_sinkisinkj*kx*kz !zx

                        gradmu_efield(jy,iy) = gradmu_efield(jy,iy) + &
                              Sk_alpha*coskicoskj_p_sinkisinkj*ky*ky !yy
                        gradmu_efield(jz,iy) = gradmu_efield(jz,iy) + &
                              Sk_alpha*coskicoskj_p_sinkisinkj*ky*kz !zy

                        gradmu_efield(jz,iz) = gradmu_efield(jz,iz) + &
                              Sk_alpha*coskicoskj_p_sinkisinkj*kz*kz !zz
                     end do
                  end do ! ni
               end if
            end do ! imode
         end do
      end do

   end subroutine gradmumelt_electricfield

   !================================================================================
   ! Compute the long-range Coulomb potential felt by each electrode
   ! atoms due to all melt ions
   subroutine mumelt2Qelec_potential(localwork, ewald, box, &
      ions, electrodes, dipoles, V)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork  !< Local work assignment
      type(MW_box_t),       intent(in)    :: box        !< Simulation box parameters
      type(MW_ion_t),       intent(in)    :: ions(:)        !< ions parameters
      type(MW_electrode_t), intent(in)    :: electrodes(:)   !< electrode parameters
      real(wp),             intent(in)    :: dipoles(:,:)       !< ions dipoles

      ! Parameters out
      ! --------------
      type(MW_ewald_t),     intent(inout) :: ewald      !< Ewald summation parameters
      real(wp),             intent(inout) :: V(:)       !< Coulomb potential

      ! Local
      ! -----
      integer :: num_ion_types, num_elec_types
      integer :: i, itype
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: muix, muiy, muiz !< ion charge and dipoles
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: volfactor
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky, cos_kxkykz, sin_kxkykz
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec
      real(wp) :: skmux_cos, skmux_sin
      real(wp) :: skmuy_cos, skmuy_sin
      real(wp) :: skmuz_cos, skmuz_sin
      real(wp) :: kdotmucos, kdotmusin

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ion_types = size(ions,1)
      num_elec_types = size(electrodes,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha * alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      volfactor = 2.0_wp/box%area(3)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_pbc = ewald%num_pbc

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ! Initiliaze potential to 0
      ! -------------------------
      V(:) = 0.0_wp

      !Melt dipoles
      ewald%Skmux_cos(:) = 0.0_wp
      ewald%Skmux_sin(:) = 0.0_wp
      ewald%Skmuy_cos(:) = 0.0_wp
      ewald%Skmuy_sin(:) = 0.0_wp
      ewald%Skmuz_cos(:) = 0.0_wp
      ewald%Skmuz_sin(:) = 0.0_wp

      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes in the exectude globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank

      ! Setup cache blocking parameters
      do itype = 1, num_ion_types
         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m), wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n), wp)
               kz = ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  !Melt dipoles contribution to structure factor
                  skmux_cos = 0.0_wp
                  skmux_sin = 0.0_wp
                  skmuy_cos = 0.0_wp
                  skmuy_sin = 0.0_wp
                  skmuz_cos = 0.0_wp
                  skmuz_sin = 0.0_wp

                  do i = istart_block, iend_block

                     muix = dipoles(i,1)
                     muiy = dipoles(i,2)
                     muiz = dipoles(i,3)

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     skmux_cos = skmux_cos + muix*cos_kxkykz
                     skmux_sin = skmux_sin + muix*sin_kxkykz
                     skmuy_cos = skmuy_cos + muiy*cos_kxkykz
                     skmuy_sin = skmuy_sin + muiy*sin_kxkykz
                     skmuz_cos = skmuz_cos + muiz*cos_kxkykz
                     skmuz_sin = skmuz_sin + muiz*sin_kxkykz

                  end do

                  !Melt dipoles
                  ewald%Skmux_cos(imode) = ewald%Skmux_cos(imode) + skmux_cos
                  ewald%Skmux_sin(imode) = ewald%Skmux_sin(imode) + skmux_sin
                  ewald%Skmuy_cos(imode) = ewald%Skmuy_cos(imode) + skmuy_cos
                  ewald%Skmuy_sin(imode) = ewald%Skmuy_sin(imode) + skmuy_sin
                  ewald%Skmuz_cos(imode) = ewald%Skmuz_cos(imode) + skmuz_cos
                  ewald%Skmuz_sin(imode) = ewald%Skmuz_sin(imode) + skmuz_sin

               end if
            end do
         end do
      end do

      ! Setup cache blocking parameters
      do itype = 1, num_elec_types
         num_blocks = 0
         if (electrodes(itype)%count > 0) then
            num_blocks = (electrodes(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = electrodes(itype)%offset + electrodes(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + 1 + electrodes(itype)%offset
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            num_modes = localwork%ewald_num_mode_local
            l = localwork%ewald_kstart_x
            m = localwork%ewald_kstart_y
            n = localwork%ewald_kstart_z

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m), wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n), wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)
                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_elec(i,l)
                     sin_kx = ewald%sin_kx_elec(i,l)
                     cos_ky = ewald%cos_ky_elec(i,mabs)
                     sin_ky = ewald%sin_ky_elec(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_elec(i,nabs)
                     sin_kz = ewald%sin_kz_elec(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     kdotmucos = ewald%Skmux_cos(imode)*kx + ewald%Skmuy_cos(imode)*ky + ewald%Skmuz_cos(imode)*kz   !rdotmuc in pimaim
                     kdotmusin = ewald%Skmux_sin(imode)*kx + ewald%Skmuy_sin(imode)*ky + ewald%Skmuz_sin(imode)*kz   !rdotmus in pimaim

                     V(i) = V(i) + Sk_alpha*(sin_kxkykz*kdotmucos - cos_kxkykz*kdotmusin)
                  end do
               end if
            end do
         end do
      end do
   end subroutine mumelt2Qelec_potential


   !================================================================================
   ! Compute the electric field felt by each melt ions due to other melt ions charges
   subroutine qmelt2mumelt_electricfield(localwork, ewald, box, ions, efield)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork       !< Local work assignment
      type(MW_box_t),       intent(in)    :: box             !< Simulation box parameters
      type(MW_ion_t),       intent(in)    :: ions(:)         !< ions parameters

      ! Parameters out
      ! --------------
      type(MW_ewald_t),     intent(inout) :: ewald           !< Ewald summation parameters
      real(wp),             intent(inout) :: efield(:,:) !< Electric field on ions

      ! Local
      ! -----
      integer :: num_ion_types
      integer :: i, itype
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: qi
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky, cos_kxkykz, sin_kxkykz
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec
      real(wp) :: sk_cos, sk_sin

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ion_types = size(ions,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ewald%Sk_cos(:) = 0.0_wp
      ewald%Sk_sin(:) = 0.0_wp

      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes executed globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank
      volfactor = 2.0_wp/box%area(3)

      !ions
      ! Setup cache blocking parameters
      do itype = 1, num_ion_types
         qi = ions(itype)%charge
         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m * twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= knorm2_max) then

                  ! Ions contribution to structure factor
                  sk_cos = 0.0_wp
                  sk_sin = 0.0_wp
                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     sk_cos = sk_cos + qi*cos_kxkykz
                     sk_sin = sk_sin + qi*sin_kxkykz
                  end do
                  ewald%Sk_cos(imode) = ewald%Sk_cos(imode) + sk_cos
                  ewald%Sk_sin(imode) = ewald%Sk_sin(imode) + sk_sin

               end if
            end do ! imode
         end do
      end do

      !ions2ions
      ! Setup cache blocking parameters
      do itype = 1, num_ion_types
         qi = ions(itype)%charge

         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)

                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     ! electric field
                     efield(i,1) = efield(i,1) +  kx*Sk_alpha*(sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))
                     efield(i,2) = efield(i,2) +  ky*Sk_alpha*(sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))
                     efield(i,3) = efield(i,3) +  kz*Sk_alpha*(sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))

                  end do ! ni
               end if
            end do ! imode
         end do
      end do

   end subroutine qmelt2mumelt_electricfield

   !================================================================================
   ! Compute the electric fields felt by each melt ions due to other melt ions dipoles
   subroutine mumelt2mumelt_electricfield(localwork, ewald, box, ions, efield, dipoles)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork       !< Local work assignment
      type(MW_box_t),       intent(in)    :: box             !< Simulation box parameters
      type(MW_ion_t),       intent(in)    :: ions(:)         !< ions parameters
      real(wp),             intent(in)    :: dipoles(:,:)       !< ions dipoles

      ! Parameters out
      ! --------------
      type(MW_ewald_t),     intent(inout) :: ewald           !< Ewald summation parameters
      real(wp),             intent(inout) :: efield(:,:) !< Electric field on ions

      ! Local
      ! -----
      integer :: num_ion_types
      integer :: i, itype
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky, cos_kxkykz, sin_kxkykz
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec
      real(wp) :: skmux_cos, skmux_sin
      real(wp) :: skmuy_cos, skmuy_sin
      real(wp) :: skmuz_cos, skmuz_sin
      real(wp) :: kdotmucos,kdotmusin

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ion_types = size(ions,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ewald%Skmux_cos(:) = 0.0_wp
      ewald%Skmux_sin(:) = 0.0_wp
      ewald%Skmuy_cos(:) = 0.0_wp
      ewald%Skmuy_sin(:) = 0.0_wp
      ewald%Skmuz_cos(:) = 0.0_wp
      ewald%Skmuz_sin(:) = 0.0_wp

      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes executed globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank
      volfactor = 2.0_wp/box%area(3)

      ! Setup cache blocking parameters
      do itype = 1, num_ion_types

         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= knorm2_max) then

                  ! Ions contribution to structure factor
                  skmux_cos = 0.0_wp
                  skmux_sin = 0.0_wp
                  skmuy_cos = 0.0_wp
                  skmuy_sin = 0.0_wp
                  skmuz_cos = 0.0_wp
                  skmuz_sin = 0.0_wp
                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     skmux_cos = skmux_cos + dipoles(i,1)*cos_kxkykz
                     skmux_sin = skmux_sin + dipoles(i,1)*sin_kxkykz
                     skmuy_cos = skmuy_cos + dipoles(i,2)*cos_kxkykz
                     skmuy_sin = skmuy_sin + dipoles(i,2)*sin_kxkykz
                     skmuz_cos = skmuz_cos + dipoles(i,3)*cos_kxkykz
                     skmuz_sin = skmuz_sin + dipoles(i,3)*sin_kxkykz
                  end do
                  ewald%Skmux_cos(imode) = ewald%Skmux_cos(imode) + skmux_cos
                  ewald%Skmux_sin(imode) = ewald%Skmux_sin(imode) + skmux_sin
                  ewald%Skmuy_cos(imode) = ewald%Skmuy_cos(imode) + skmuy_cos
                  ewald%Skmuy_sin(imode) = ewald%Skmuy_sin(imode) + skmuy_sin
                  ewald%Skmuz_cos(imode) = ewald%Skmuz_cos(imode) + skmuz_cos
                  ewald%Skmuz_sin(imode) = ewald%Skmuz_sin(imode) + skmuz_sin

               end if
            end do ! imode
         end do
      end do

      ! Setup cache blocking parameters
      do itype = 1, num_ion_types

         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  Sk_alpha =  2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)

                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     kdotmucos = ewald%Skmux_cos(imode)*kx + ewald%Skmuy_cos(imode)*ky + ewald%Skmuz_cos(imode)*kz   !rdotmuc in pimaim
                     kdotmusin = ewald%Skmux_sin(imode)*kx + ewald%Skmuy_sin(imode)*ky + ewald%Skmuz_sin(imode)*kz   !rdotmus in pimaim

                     ! electric field
                     efield(i,1)  = efield(i,1) - Sk_alpha * (cos_kxkykz*kdotmucos + sin_kxkykz*kdotmusin)*kx
                     efield(i,2)  = efield(i,2) - Sk_alpha * (cos_kxkykz*kdotmucos + sin_kxkykz*kdotmusin)*ky
                     efield(i,3)  = efield(i,3) - Sk_alpha * (cos_kxkykz*kdotmucos + sin_kxkykz*kdotmusin)*kz

                  end do ! ni
               end if
            end do ! imode
         end do
      end do

   end subroutine mumelt2mumelt_electricfield

   !================================================================================
   ! Compute the electric field felt by each melt ions due to other melt ions charges
   subroutine Qelec2mumelt_electricfield(localwork, ewald, box, ions, &
      electrodes, q_elec, efield)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork       !< Local work assignment
      type(MW_box_t),       intent(in)    :: box             !< Simulation box parameters
      type(MW_ion_t),       intent(in)    :: ions(:)         !< ions parameters
      type(MW_electrode_t), intent(in)    :: electrodes(:)   !< electrodes parameters
      real(wp),             intent(in)    :: q_elec(:)       !< Electrode atoms charge

      ! Parameters out
      ! --------------
      type(MW_ewald_t),     intent(inout) :: ewald           !< Ewald summation parameters
      real(wp),             intent(inout) :: efield(:,:) !< Electric field on ions

      ! Local
      ! -----
      integer :: num_ion_types, num_elec_types
      integer :: i, itype
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: Q_i !< electrode charges
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky, cos_kxkykz, sin_kxkykz
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec
      real(wp) :: sk_cos, sk_sin

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ion_types = size(ions,1)
      num_elec_types = size(electrodes,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ewald%Sk_cos(:) = 0.0_wp
      ewald%Sk_sin(:) = 0.0_wp

      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes executed globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank
      volfactor = 2.0_wp/box%area(3)

      !electrodes
      do itype = 1, num_elec_types
         num_blocks = 0
         if (electrodes(itype)%count > 0) then
            num_blocks = (electrodes(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = electrodes(itype)%offset + electrodes(itype)%count

         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + 1 + electrodes(itype)%offset
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then
                  sk_cos = 0.0_wp
                  sk_sin = 0.0_wp
                  ! Atoms contribution to structure factor
                  do i = istart_block, iend_block
                     cos_kx = ewald%cos_kx_elec(i,l)
                     sin_kx = ewald%sin_kx_elec(i,l)
                     cos_ky = ewald%cos_ky_elec(i,mabs)
                     sin_ky = ewald%sin_ky_elec(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_elec(i,nabs)
                     sin_kz = ewald%sin_kz_elec(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     Q_i = q_elec(i)

                     sk_cos = sk_cos + Q_i*cos_kxkykz
                     sk_sin = sk_sin + Q_i*sin_kxkykz
                  end do
                  ewald%Sk_cos(imode) = ewald%Sk_cos(imode) + sk_cos
                  ewald%Sk_sin(imode) = ewald%Sk_sin(imode) + sk_sin
               end if
            end do ! imode
         end do
      end do

      !electric field
      ! Setup cache blocking parameters
      do itype = 1, num_ion_types
         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)

                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     ! electric field
                     efield(i,1) = efield(i,1) + kx*Sk_alpha* &
                           (sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))
                     efield(i,2) = efield(i,2) + ky*Sk_alpha* &
                           (sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))
                     efield(i,3) = efield(i,3) + kz*Sk_alpha* &
                           (sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))

                  end do ! ni
               end if
            end do ! imode
         end do
      end do

   end subroutine Qelec2mumelt_electricfield

   !================================================================================
   ! Compute the Coulomb forces felt by each melt ions due to other melt ions
   subroutine melt_forces(localwork, ewald, box, ions, &
      electrodes, q_elec, force, stress_tensor, dipoles)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in)    :: localwork       !< Local work assignment
      type(MW_box_t),       intent(in)    :: box             !< Simulation box parameters
      type(MW_ion_t),       intent(in)    :: ions(:)         !< ions parameters
      type(MW_electrode_t), intent(in)    :: electrodes(:)   !< electrodes parameters
      real(wp),             intent(in)    :: q_elec(:)       !< Electrode atoms charge
      real(wp),             intent(in)    :: dipoles(:,:)       !< ions dipoles

      ! Parameters out
      ! --------------
      type(MW_ewald_t),     intent(inout) :: ewald           !< Ewald summation parameters
      real(wp),             intent(inout) :: force(:,:) !< Coulomb force on ions
      real(wp),             intent(inout) :: stress_tensor(:,:) !< Coulomb force contribution to stress

      ! Local
      ! -----
      integer :: num_ion_types, num_elec_types
      integer :: i, itype
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: qi
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_alpha !< Structure factor coefficients
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky, cos_kxkykz, sin_kxkykz
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec
      real(wp) :: skq_cos, skq_sin
      real(wp) :: skmux_cos, skmux_sin
      real(wp) :: skmuy_cos, skmuy_sin
      real(wp) :: skmuz_cos, skmuz_sin
      real(wp) :: kdotmucos, kdotmusin
      real(wp) :: kdotmucosxx, kdotmusinxx
      real(wp) :: kdotmucosyy, kdotmusinyy
      real(wp) :: kdotmucoszz, kdotmusinzz
      real(wp) :: kdotmucosxy, kdotmusinxy
      real(wp) :: kdotmucosxz, kdotmusinxz
      real(wp) :: kdotmucosyz, kdotmusinyz
      real(wp) :: fqq, fqmu, fmuq, fmumu, ftot
      real(wp) :: stfac1,stfac2,qcos,qsin

      integer :: imode, num_modes, mode_offset
      integer :: iblock, num_blocks, istart_block, iend_block, iend_type
      integer :: num_pbc

      num_ion_types = size(ions,1)
      num_elec_types = size(electrodes,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ewald%Sk_cos(:) = 0.0_wp
      ewald%Sk_sin(:) = 0.0_wp
      ewald%Skmux_cos(:) = 0.0_wp
      ewald%Skmux_sin(:) = 0.0_wp
      ewald%Skmuy_cos(:) = 0.0_wp
      ewald%Skmuy_sin(:) = 0.0_wp
      ewald%Skmuz_cos(:) = 0.0_wp
      ewald%Skmuz_sin(:) = 0.0_wp

      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes executed globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank
      volfactor = 2.0_wp/box%area(3)

      ! Setup cache blocking parameters
      do itype = 1, num_ion_types
         qi = ions(itype)%charge

         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= knorm2_max) then

                  ! Ions contribution to structure factor
                  ! melt charges contribution
                  skq_cos = 0.0_wp
                  skq_sin = 0.0_wp
                  ! melt dipoles contribution
                  skmux_cos = 0.0_wp
                  skmux_sin = 0.0_wp
                  skmuy_cos = 0.0_wp
                  skmuy_sin = 0.0_wp
                  skmuz_cos = 0.0_wp
                  skmuz_sin = 0.0_wp
                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     skq_cos = skq_cos + qi*cos_kxkykz
                     skq_sin = skq_sin + qi*sin_kxkykz

                     skmux_cos = skmux_cos + dipoles(i,1)*cos_kxkykz
                     skmux_sin = skmux_sin + dipoles(i,1)*sin_kxkykz
                     skmuy_cos = skmuy_cos + dipoles(i,2)*cos_kxkykz
                     skmuy_sin = skmuy_sin + dipoles(i,2)*sin_kxkykz
                     skmuz_cos = skmuz_cos + dipoles(i,3)*cos_kxkykz
                     skmuz_sin = skmuz_sin + dipoles(i,3)*sin_kxkykz
                  end do
                  ewald%Sk_cos(imode) = ewald%Sk_cos(imode) + skq_cos
                  ewald%Sk_sin(imode) = ewald%Sk_sin(imode) + skq_sin

                  ewald%Skmux_cos(imode) = ewald%Skmux_cos(imode) + skmux_cos
                  ewald%Skmux_sin(imode) = ewald%Skmux_sin(imode) + skmux_sin
                  ewald%Skmuy_cos(imode) = ewald%Skmuy_cos(imode) + skmuy_cos
                  ewald%Skmuy_sin(imode) = ewald%Skmuy_sin(imode) + skmuy_sin
                  ewald%Skmuz_cos(imode) = ewald%Skmuz_cos(imode) + skmuz_cos
                  ewald%Skmuz_sin(imode) = ewald%Skmuz_sin(imode) + skmuz_sin

               end if
            end do ! imode
         end do
      end do

      do itype = 1, num_elec_types
         num_blocks = 0
         if (electrodes(itype)%count > 0) then
            num_blocks = (electrodes(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = electrodes(itype)%offset + electrodes(itype)%count

         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + 1 + electrodes(itype)%offset
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then
                  ! electrode charges contribution
                  skq_cos = 0.0_wp
                  skq_sin = 0.0_wp
                  ! Atoms contribution to structure factor
                  do i = istart_block, iend_block
                     cos_kx = ewald%cos_kx_elec(i,l)
                     sin_kx = ewald%sin_kx_elec(i,l)
                     cos_ky = ewald%cos_ky_elec(i,mabs)
                     sin_ky = ewald%sin_ky_elec(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_elec(i,nabs)
                     sin_kz = ewald%sin_kz_elec(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     skq_cos = skq_cos + q_elec(i)*cos_kxkykz
                     skq_sin = skq_sin + q_elec(i)*sin_kxkykz
                  end do
                  !Sk_cos/sin have inside the contribution of melt and electrodes charges
                  ewald%Sk_cos(imode) = ewald%Sk_cos(imode) + skq_cos
                  ewald%Sk_sin(imode) = ewald%Sk_sin(imode) + skq_sin
               end if
            end do ! imode
         end do
      end do

      ! Setup cache blocking parameters
      do itype = 1, num_ion_types
         qi = ions(itype)%charge

         num_blocks = 0
         if (ions(itype)%count > 0) then
            num_blocks = (ions(itype)%count-1)/block_vector_size + 1
         end if
         iend_type = ions(itype)%offset + ions(itype)%count
         do iblock = 1, num_blocks
            istart_block = (iblock-1)*block_vector_size + ions(itype)%offset + 1
            iend_block = min(istart_block+block_vector_size-1,iend_type)

            do imode = 1, num_modes
               call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
               ! kx = l * twopi / box%length(1)
               kx = (real(l,wp)*twopi)*box_lengthx_rec
               ! ky = m*twopi / box%length(2)
               mabs = abs(m)
               sign_m = real(sign(1,m),wp)
               ky = (real(m,wp)*twopi)*box_lengthy_rec
               ! kz = zpoint(n)
               nabs = abs(n)
               sign_n = real(sign(1,n),wp)
               kz = sign_n*ewald%zpoint(nabs)

               ! Norm square of the k-mode vector
               knorm2 = kx*kx + ky*ky + kz*kz
               if (knorm2 <= ewald%knorm2_max) then

                  Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)

                  kdotmucos = ewald%Skmux_cos(imode)*kx + ewald%Skmuy_cos(imode)*ky + ewald%Skmuz_cos(imode)*kz   !rdotmuc in pimaim
                  kdotmusin = ewald%Skmux_sin(imode)*kx + ewald%Skmuy_sin(imode)*ky + ewald%Skmuz_sin(imode)*kz   !rdotmus in pimaim

                  ! Computing forces over melt
                  do i = istart_block, iend_block

                     cos_kx = ewald%cos_kx_ions(i,l)
                     sin_kx = ewald%sin_kx_ions(i,l)
                     cos_ky = ewald%cos_ky_ions(i,mabs)
                     sin_ky = ewald%sin_ky_ions(i,mabs)*sign_m
                     cos_kz = ewald%cos_kz_ions(i,nabs)
                     sin_kz = ewald%sin_kz_ions(i,nabs)*sign_n

                     cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                     sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                     cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                     sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                     ! force on melt
                     fqq   = qi*Sk_alpha*(sin_kxkykz*ewald%Sk_cos(imode) - cos_kxkykz*ewald%Sk_sin(imode))
                     fqmu  = qi*Sk_alpha*(-cos_kxkykz*kdotmucos - sin_kxkykz*kdotmusin)
                     fmuq  = (dipoles(i,1)*kx + dipoles(i,2)*ky + dipoles(i,3)*kz)*Sk_alpha* &
                           (cos_kxkykz*ewald%Sk_cos(imode) + sin_kxkykz*ewald%Sk_sin(imode))
                     fmumu = (dipoles(i,1)*kx + dipoles(i,2)*ky + dipoles(i,3)*kz)*Sk_alpha* &
                           (-cos_kxkykz*kdotmusin + sin_kxkykz*kdotmucos)
                     ftot  = fqq + fqmu + fmuq + fmumu

                     force(i,1) = force(i,1) + ftot*kx
                     force(i,2) = force(i,2) + ftot*ky
                     force(i,3) = force(i,3) + ftot*kz

                  end do ! ni
               end if
            end do ! imode
         end do
      end do

      ! Setup cache blocking parameters
      do imode = 1, num_modes
         call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
         ! kx = l * twopi / box%length(1)
         kx = (real(l,wp)*twopi)*box_lengthx_rec
         ! ky = m*twopi / box%length(2)
         mabs = abs(m)
         sign_m = real(sign(1,m),wp)
         ky = (real(m,wp)*twopi)*box_lengthy_rec
         ! kz = zpoint(n)
         nabs = abs(n)
         sign_n = real(sign(1,n),wp)
         kz = sign_n*ewald%zpoint(nabs)

         ! Norm square of the k-mode vector
         knorm2 = kx*kx + ky*ky + kz*kz
         if (knorm2 <= ewald%knorm2_max) then

            Sk_alpha = volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)  ! divided by 2 compared to forces

            qcos = ewald%Sk_cos(imode)
            qsin = ewald%Sk_sin(imode)
            kdotmucos = ewald%Skmux_cos(imode)*kx + ewald%Skmuy_cos(imode)*ky + ewald%Skmuz_cos(imode)*kz   !rdotmuc in pimaim
            kdotmusin = ewald%Skmux_sin(imode)*kx + ewald%Skmuy_sin(imode)*ky + ewald%Skmuz_sin(imode)*kz   !rdotmus in pimaim

            kdotmucosxx = ewald%Skmux_cos(imode)*kx    
            kdotmusinxx = ewald%Skmux_sin(imode)*kx    

            kdotmucosyy = ewald%Skmuy_cos(imode)*ky    
            kdotmusinyy = ewald%Skmuy_sin(imode)*ky    

            kdotmucoszz = ewald%Skmuz_cos(imode)*kz    
            kdotmusinzz = ewald%Skmuz_sin(imode)*kz    

            kdotmucosxy = 0.5_wp*(ewald%Skmux_cos(imode)*ky + ewald%Skmuy_cos(imode)*kx)   
            kdotmusinxy = 0.5_wp*(ewald%Skmux_sin(imode)*ky + ewald%Skmuy_sin(imode)*kx)   

            kdotmucosxz = 0.5_wp*(ewald%Skmux_cos(imode)*kz + ewald%Skmuz_cos(imode)*kx)   
            kdotmusinxz = 0.5_wp*(ewald%Skmux_sin(imode)*kz + ewald%Skmuz_sin(imode)*kx)   

            kdotmucosyz = 0.5_wp*(ewald%Skmuy_cos(imode)*kz + ewald%Skmuz_cos(imode)*ky)   
            kdotmusinyz = 0.5_wp*(ewald%Skmuy_sin(imode)*kz + ewald%Skmuz_sin(imode)*ky)   

            ! q q term
            stfac1 = (4.0_wp*alphasq+knorm2)/(4.0_wp*alphasq*knorm2)   
            stfac2 = Sk_alpha * (qcos**2.0+qsin**2.0)

            stress_tensor(1,1)=stress_tensor(1,1)+ &
                     (1.0_wp-2.0_wp * kx * kx * stfac1)*stfac2 
            stress_tensor(2,2)=stress_tensor(2,2)+ &
                     (1.0_wp-2.0_wp * ky * ky * stfac1)*stfac2 
            stress_tensor(3,3)=stress_tensor(3,3)+ &
                     (1.0_wp-2.0_wp * kz * kz * stfac1)*stfac2 
            stress_tensor(1,2)=stress_tensor(1,2)+ &
                     (-2.0_wp * kx * ky * stfac1)*stfac2 
            stress_tensor(1,3)=stress_tensor(1,3)+ &
                     (-2.0_wp * kx * kz * stfac1)*stfac2 
            stress_tensor(2,3)=stress_tensor(2,3)+ &
                     (-2.0_wp * ky * kz * stfac1)*stfac2

            ! q mu term             
            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucos - qcos * kdotmusin) 

            stress_tensor(1,1)=stress_tensor(1,1)+ &
                     (1.0_wp-2.0_wp * kx * kx * stfac1)*stfac2 
            stress_tensor(2,2)=stress_tensor(2,2)+ &
                     (1.0_wp-2.0_wp * ky * ky * stfac1)*stfac2 
            stress_tensor(3,3)=stress_tensor(3,3)+ &
                     (1.0_wp-2.0_wp * kz * kz * stfac1)*stfac2 
            stress_tensor(1,2)=stress_tensor(1,2)+ &
                     (-2.0_wp * kx * ky * stfac1)*stfac2 
            stress_tensor(1,3)=stress_tensor(1,3)+ &
                     (-2.0_wp * kx * kz * stfac1)*stfac2 
            stress_tensor(2,3)=stress_tensor(2,3)+ &
                     (-2.0_wp * ky * kz * stfac1)*stfac2

            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucosxx - qcos * kdotmusinxx) 
            stress_tensor(1,1)=stress_tensor(1,1)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucosyy - qcos * kdotmusinyy) 
            stress_tensor(2,2)=stress_tensor(2,2)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucoszz - qcos * kdotmusinzz) 
            stress_tensor(3,3)=stress_tensor(3,3)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucosxy - qcos * kdotmusinxy) 
            stress_tensor(1,2)=stress_tensor(1,2)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucosxz - qcos * kdotmusinxz) 
            stress_tensor(1,3)=stress_tensor(1,3)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (qsin * kdotmucosyz - qcos * kdotmusinyz) 
            stress_tensor(2,3)=stress_tensor(2,3)+stfac2 
            
            ! mu mu term             
            stfac2 = Sk_alpha * (kdotmucos**2.0 + kdotmusin**2.0) 

            stress_tensor(1,1)=stress_tensor(1,1)+ &
                     (1.0_wp-2.0_wp * kx * kx * stfac1)*stfac2 
            stress_tensor(2,2)=stress_tensor(2,2)+ &
                     (1.0_wp-2.0_wp * ky * ky * stfac1)*stfac2 
            stress_tensor(3,3)=stress_tensor(3,3)+ &
                     (1.0_wp-2.0_wp * kz * kz * stfac1)*stfac2 
            stress_tensor(1,2)=stress_tensor(1,2)+ &
                     (-2.0_wp * kx * ky * stfac1)*stfac2 
            stress_tensor(1,3)=stress_tensor(1,3)+ &
                     (-2.0_wp * kx * kz * stfac1)*stfac2 
            stress_tensor(2,3)=stress_tensor(2,3)+ &
                     (-2.0_wp * ky * kz * stfac1)*stfac2

            stfac2 = 2.0_wp*Sk_alpha * (kdotmucos * kdotmucosxx + kdotmusin * kdotmusinxx) 
            stress_tensor(1,1)=stress_tensor(1,1)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (kdotmucos * kdotmucosyy + kdotmusin * kdotmusinyy) 
            stress_tensor(2,2)=stress_tensor(2,2)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (kdotmucos * kdotmucoszz + kdotmusin * kdotmusinzz) 
            stress_tensor(3,3)=stress_tensor(3,3)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (kdotmucos * kdotmucosxy + kdotmusin * kdotmusinxy) 
            stress_tensor(1,2)=stress_tensor(1,2)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (kdotmucos * kdotmucosxz + kdotmusin * kdotmusinxz) 
            stress_tensor(1,3)=stress_tensor(1,3)+stfac2 
            
            stfac2 = 2.0_wp*Sk_alpha * (kdotmucos * kdotmucosyz + kdotmusin * kdotmusinyz) 
            stress_tensor(2,3)=stress_tensor(2,3)+stfac2 
            
         end if
      end do ! imode

   end subroutine melt_forces

   !================================================================================
   ! Compute the contibution to the energy from long-range Coulomb interaction
   subroutine energy(localwork, ewald, box, ions, &
      electrodes, q_elec, h, dipoles)
      implicit none

      ! Parameters in
      ! -------------
      type(MW_localwork_t), intent(in) :: localwork       !< Local work assignment
      type(MW_ewald_t),     intent(in) :: ewald           !< Ewald summation parameters
      type(MW_box_t),       intent(in) :: box             !< Simulation box parameters
      type(MW_ion_t),       intent(in) :: ions(:)         !< ions parameters
      type(MW_electrode_t), intent(in) :: electrodes(:)   !< electrodes parameters
      real(wp),             intent(in) :: q_elec(:)       !< Electrode atoms charge
      real(wp),             intent(in) :: dipoles(:,:)    !< ions dipoles

      ! Parameters out
      ! --------------
      real(wp), intent(inout) :: h !< energy

      ! Local
      ! -----
      integer :: num_ion_types, num_elec_types
      integer :: i, itype, iion
      integer :: l, m, n, mabs, nabs
      integer :: kmax_x, kmax_y, kmax_z
      real(wp) :: sign_m, sign_n
      real(wp) :: qi
      real(wp) :: kx, ky, kz, knorm2, knorm2_max
      real(wp) :: Sk_cos, Sk_sin, Sk_alpha , Sk_cos_type, Sk_sin_type !< Structure factor coefficients
      real(wp) :: Skmu_cos,Skmu_sin,Skmu_cos_type,Skmu_sin_type
      real(wp) :: kdotmu
      real(wp) :: cos_kx, sin_kx, cos_ky, sin_ky, cos_kz, sin_kz
      real(wp) :: cos_kxky, sin_kxky, cos_kxkykz, sin_kxkykz
      real(wp) :: volfactor
      real(wp) :: alpha, alphasq, alphaconst
      real(wp) :: box_lengthx_rec, box_lengthy_rec

      integer :: imode, num_modes, mode_offset
      integer :: num_pbc

      num_ion_types = size(ions,1)
      num_elec_types = size(electrodes,1)

      ! Precompute some factors
      ! -----------------------
      alpha = ewald%alpha
      alphasq = alpha*alpha
      alphaconst = -1.0_wp/(4.0_wp*alphasq)

      num_pbc = ewald%num_pbc
      box_lengthx_rec = 1.0_wp/box%length(1)
      box_lengthy_rec = 1.0_wp/box%length(2)

      knorm2_max = ewald%knorm2_max
      kmax_x = ewald%kmax_x
      kmax_y = ewald%kmax_y
      kmax_z = ewald%kmax_z

      num_modes = localwork%ewald_num_mode_local
      mode_offset = localwork%ewald_local_mode_offset


      ! Long range contribution is computed in reciprocal space
      ! -------------------------------------------------------
      ! The sum over k-modes is symmetric
      !   -> we loop only on half of them and mulitply by 2.0
      ! Total number of modes
      !    (2*kmax_x+1)*(2*kmax_y+1)*nz
      ! Num modes executed globally
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz
      ! Num modes executed by the local process (approx.)
      !    (2*kmax_x*kmax_y + kmax_x + kmax_y)*nz / num_rank
      volfactor = 1.0_wp/box%area(3)

      do imode = 1, num_modes
         call compute_kmode_index(num_pbc, kmax_x, kmax_y, kmax_z, imode+mode_offset, l, m, n)
         ! kx = l * twopi / box%length(1)
         kx = (real(l,wp)*twopi)*box_lengthx_rec
         ! ky = m*twopi / box%length(2)
         mabs = abs(m)
         sign_m = real(sign(1,m),wp)
         ky = (real(m,wp)*twopi)*box_lengthy_rec
         ! kz = zpoint(n)
         nabs = abs(n)
         sign_n = real(sign(1,n),wp)
         kz = sign_n*ewald%zpoint(nabs)

         ! Norm square of the k-mode vector
         knorm2 = kx*kx + ky*ky + kz*kz
         if (knorm2 <= ewald%knorm2_max) then
            !Melt charges
            Sk_cos = 0.0_wp
            Sk_sin = 0.0_wp
            !Melt dipoles
            Skmu_cos = 0.0_wp
            Skmu_sin = 0.0_wp
            ! Melt contribution to structure factor
            do itype = 1, num_ion_types
               qi = ions(itype)%charge
               Sk_cos_type = 0.0_wp
               Sk_sin_type = 0.0_wp
               Skmu_cos_type = 0.0_wp
               Skmu_sin_type = 0.0_wp
               do i = 1, ions(itype)%count
                  iion = ions(itype)%offset + i
                  cos_kx = ewald%cos_kx_ions(iion,l)
                  sin_kx = ewald%sin_kx_ions(iion,l)
                  cos_ky = ewald%cos_ky_ions(iion,mabs)
                  sin_ky = ewald%sin_ky_ions(iion,mabs)*sign_m
                  cos_kz = ewald%cos_kz_ions(iion,nabs)
                  sin_kz = ewald%sin_kz_ions(iion,nabs)*sign_n

                  cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                  sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                  cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                  sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                  Sk_cos_type = Sk_cos_type + qi*cos_kxkykz
                  Sk_sin_type = Sk_sin_type + qi*sin_kxkykz

                  kdotmu = dipoles(iion,1)*kx + dipoles(iion,2)*ky + dipoles(iion,3)*kz

                  Skmu_cos_type = Skmu_cos_type + kdotmu*cos_kxkykz
                  Skmu_sin_type = Skmu_sin_type + kdotmu*sin_kxkykz
               end do
               Sk_cos = Sk_cos + Sk_cos_type
               Sk_sin = Sk_sin + Sk_sin_type
               Skmu_cos = Skmu_cos + Skmu_cos_type
               Skmu_sin = Skmu_sin + Skmu_sin_type
            end do

            ! Electrodes contribution to structure factor
            do itype = 1, num_elec_types
               !Electrode charges
               Sk_cos_type = 0.0_wp
               Sk_sin_type = 0.0_wp
               do i = 1, electrodes(itype)%count
                  iion = electrodes(itype)%offset + i
                  cos_kx = ewald%cos_kx_elec(iion,l)
                  sin_kx = ewald%sin_kx_elec(iion,l)
                  cos_ky = ewald%cos_ky_elec(iion,mabs)
                  sin_ky = ewald%sin_ky_elec(iion,mabs)*sign_m
                  cos_kz = ewald%cos_kz_elec(iion,nabs)
                  sin_kz = ewald%sin_kz_elec(iion,nabs)*sign_n

                  cos_kxky = cos_kx*cos_ky - sin_kx*sin_ky
                  sin_kxky = sin_kx*cos_ky + cos_kx*sin_ky

                  cos_kxkykz = cos_kxky*cos_kz - sin_kxky*sin_kz
                  sin_kxkykz = sin_kxky*cos_kz + cos_kxky*sin_kz

                  Sk_cos_type = Sk_cos_type + q_elec(iion)*cos_kxkykz
                  Sk_sin_type = Sk_sin_type + q_elec(iion)*sin_kxkykz
               end do
               Sk_cos = Sk_cos + Sk_cos_type
               Sk_sin = Sk_sin + Sk_sin_type
            end do
            Sk_alpha = 2.0_wp*volfactor*ewald%zweight(nabs)*(exp(alphaconst*knorm2)/knorm2)
            !Sk_cos/sin have inside the contribution of melt and electrodes charges
            !E = (q+Q)*(q+Q) + (q+Q)*mu + mu*mu
            h = h + Sk_alpha*(Sk_cos*Sk_cos + Sk_sin*Sk_sin + &
                  2.0_wp*Sk_sin*Skmu_cos - 2.0_wp*Sk_cos*Skmu_sin + &
                  Skmu_cos*Skmu_cos + Skmu_sin*Skmu_sin)
         end if
      end do ! imode
   end subroutine energy

   include 'update_kmode_index.inc'

end module MW_coulomb_lr_pim
