! ==============================================================================
! Compute Fumi-Tosi forces felt by melt ions
!DEC$ ATTRIBUTES NOINLINE :: melt_forces_@PBC@
subroutine melt_forces_@PBC@(localwork, ft, box, ions, xyz_ions, &
      electrodes, xyz_atoms, force, stress_tensor)
   implicit none

   ! Parameters in
   ! -------------
   type(MW_localwork_t), intent(in) :: localwork !< Local work assignment
   type(MW_fumi_tosi_t), intent(in) :: ft      !< Fumi-Tosi potential parameters
   type(MW_box_t),   intent(in) :: box     !< Simulation box parameters

   type(MW_ion_t), intent(in) :: ions(:)        !< ions parameters
   real(wp),       intent(in) :: xyz_ions(:,:)  !< ions xyz positions

   type(MW_electrode_t), intent(inout) :: electrodes(:)  !< electrode parameters
   real(wp),             intent(in) :: xyz_atoms(:,:)  !< electrode atoms xyz positions

   ! Parameters out
   ! --------------
   real(wp), intent(inout) :: force(:,:) !< Fumi-Tosi force on ions
   real(wp), intent(inout) :: stress_tensor(:,:) !< Fumi-Tosi force contribution to stress tensor

   ! Locals
   ! ------
   integer :: num_ion_types, num_elec
   integer :: itype, jtype, i, j
   real(wp) :: a, b, c, rcutsq
   real(wp) :: drnorm2, dx, dy, dz, fix, fiy, fiz
   real(wp) :: fx_melt2ele, fy_melt2ele, fz_melt2ele
   real(wp) :: ft_ij, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij
   integer :: num_block_diag, num_block_full
   integer :: iblock, iblock_offset, istart, iend, jstart, jend
   integer :: count_n, offset_n, count_m, offset_m

   real(wp) :: drnorm, drnormrec, drnorm2rec, drnorm6rec, drnorm8rec
   real(wp) :: damp_dd_sum, damp_dd_term
   real(wp) :: damp_dq_sum, damp_dq_term
   integer :: k, l

   call MW_timers_start(TIMER_VAN_DER_WAALS_MELT_FORCES)   

   num_ion_types = size(ions,1)
   num_elec = size(electrodes,1)

   a = box%length(1)
   b = box%length(2)
   c = box%length(3)

   rcutsq = ft%rcutsq

   ! Blocks on the diagonal compute only half of the pair interactions
   do itype = 1, num_ion_types
      count_n = ions(itype)%count
      offset_n = ions(itype)%offset
      do jtype = 1, itype - 1
         count_m = ions(jtype)%count
         offset_m = ions(jtype)%offset
         eta_ij = ft%eta(itype, jtype)
         B_ij = ft%B(itype, jtype)
         C_ij = ft%C(itype, jtype)
         D_ij = ft%D(itype, jtype)
         damp_dd_ij = ft%damp_dd(itype, jtype)
         damp_dq_ij = ft%damp_dq(itype, jtype)
         ! Number of blocks with full ion2ion interactions
         num_block_full = localwork%pair_ion2ion_num_block_full_local(itype, jtype)
         iblock_offset = localwork%pair_ion2ion_full_iblock_offset(itype, jtype)
         do iblock = 1, num_block_full
            call update_other_block_boundaries(iblock_offset+iblock, &
                  offset_n, count_n, offset_m, count_m, &
                  istart, iend, jstart, jend)
            do i = istart, iend
               fix = 0.0_wp
               fiy = 0.0_wp
               fiz = 0.0_wp
               do j = jstart, jend
                  call minimum_image_displacement_@PBC@(a, b, c, &
                        xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                        xyz_ions(j,1), xyz_ions(j,2), xyz_ions(j,3), &
                        dx, dy, dz, drnorm2)
                  if (drnorm2 < rcutsq) then
                     ! ft_ij = force_ij(drnorm2, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij)
                     include 'fumi_tosi_forceij.inc'


                     fix = fix +  dx * ft_ij
                     fiy = fiy +  dy * ft_ij
                     fiz = fiz +  dz * ft_ij

                     force(j,1) = force(j,1) - dx * ft_ij
                     force(j,2) = force(j,2) - dy * ft_ij
                     force(j,3) = force(j,3) - dz * ft_ij

		     stress_tensor(1,1)=stress_tensor(1,1)-dx*dx*ft_ij
		     stress_tensor(2,2)=stress_tensor(2,2)-dy*dy*ft_ij
		     stress_tensor(3,3)=stress_tensor(3,3)-dz*dz*ft_ij
		     stress_tensor(1,2)=stress_tensor(1,2)-dx*dy*ft_ij
		     stress_tensor(1,3)=stress_tensor(1,3)-dx*dz*ft_ij
		     stress_tensor(2,3)=stress_tensor(2,3)-dy*dz*ft_ij		     

                  end if
               end do
               force(i,1) = force(i,1) + fix
               force(i,2) = force(i,2) + fiy
               force(i,3) = force(i,3) + fiz
            end do
         end do
      end do

      eta_ij = ft%eta(itype, jtype)
      B_ij = ft%B(itype, jtype)
      C_ij = ft%C(itype, jtype)
      D_ij = ft%D(itype, jtype)
      damp_dd_ij = ft%damp_dd(itype, jtype)
      damp_dq_ij = ft%damp_dq(itype, jtype)

      num_block_diag = localwork%pair_ion2ion_num_block_diag_local(itype)
      iblock_offset = localwork%pair_ion2ion_diag_iblock_offset(itype)
      do iblock = 1, num_block_diag
         call update_diag_block_boundaries(iblock_offset+iblock, &
               offset_n, count_n, istart, iend)
         do i = istart, iend
            fix = 0.0_wp
            fiy = 0.0_wp
            fiz = 0.0_wp
            do j = istart, iend
               call minimum_image_displacement_@PBC@(a, b, c, &
                     xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                     xyz_ions(j,1), xyz_ions(j,2), xyz_ions(j,3), &
                     dx, dy, dz, drnorm2)
               if ((drnorm2 > 0.0_wp) .and. (drnorm2 < rcutsq)) then
                  ! ft_ij = force_ij(drnorm2, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij)
                     include 'fumi_tosi_forceij.inc'
                  fix = fix +  dx * ft_ij
                  fiy = fiy +  dy * ft_ij
                  fiz = fiz +  dz * ft_ij

		  stress_tensor(1,1)=stress_tensor(1,1)-0.5_wp*dx*dx*ft_ij
		  stress_tensor(2,2)=stress_tensor(2,2)-0.5_wp*dy*dy*ft_ij
	       	  stress_tensor(3,3)=stress_tensor(3,3)-0.5_wp*dz*dz*ft_ij
		  stress_tensor(1,2)=stress_tensor(1,2)-0.5_wp*dx*dy*ft_ij
		  stress_tensor(1,3)=stress_tensor(1,3)-0.5_wp*dx*dz*ft_ij
		  stress_tensor(2,3)=stress_tensor(2,3)-0.5_wp*dy*dz*ft_ij		  
               end if
            end do
            force(i,1) = force(i,1) + fix
            force(i,2) = force(i,2) + fiy
            force(i,3) = force(i,3) + fiz
         end do
      end do

      ! Number of blocks with full ion2ion interactions
      num_block_full = localwork%pair_ion2ion_num_block_full_local(itype,itype)
      iblock_offset = localwork%pair_ion2ion_full_iblock_offset(itype,itype)
      do iblock = 1, num_block_full
         call update_tri_block_boundaries(iblock_offset+iblock, offset_n, count_n, &
               istart, iend, jstart, jend)
         do i = istart, iend
            fix = 0.0_wp
            fiy = 0.0_wp
            fiz = 0.0_wp
            do j = jstart, jend
               call minimum_image_displacement_@PBC@(a, b, c, &
                     xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                     xyz_ions(j,1), xyz_ions(j,2), xyz_ions(j,3), &
                     dx, dy, dz, drnorm2)
               if (drnorm2 < rcutsq) then
                  ! ft_ij = force_ij(drnorm2, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij)
                     include 'fumi_tosi_forceij.inc'

                  fix = fix +  dx * ft_ij
                  fiy = fiy +  dy * ft_ij
                  fiz = fiz +  dz * ft_ij

                  force(j,1) = force(j,1) - dx * ft_ij
                  force(j,2) = force(j,2) - dy * ft_ij
                  force(j,3) = force(j,3) - dz * ft_ij

		  stress_tensor(1,1)=stress_tensor(1,1)-dx*dx*ft_ij
		  stress_tensor(2,2)=stress_tensor(2,2)-dy*dy*ft_ij
	     	  stress_tensor(3,3)=stress_tensor(3,3)-dz*dz*ft_ij
		  stress_tensor(1,2)=stress_tensor(1,2)-dx*dy*ft_ij
		  stress_tensor(1,3)=stress_tensor(1,3)-dx*dz*ft_ij
		  stress_tensor(2,3)=stress_tensor(2,3)-dy*dz*ft_ij		  
               end if
            end do
            force(i,1) = force(i,1) + fix
            force(i,2) = force(i,2) + fiy
            force(i,3) = force(i,3) + fiz
         end do
      end do

      do jtype = 1, num_elec
         count_m = electrodes(jtype)%count
         offset_m = electrodes(jtype)%offset

         eta_ij = ft%eta(itype, jtype+num_ion_types)
         B_ij = ft%B(itype, jtype+num_ion_types)
         C_ij = ft%C(itype, jtype+num_ion_types)
         D_ij = ft%D(itype, jtype+num_ion_types)
         damp_dd_ij = ft%damp_dd(itype, jtype+num_ion_types)
         damp_dq_ij = ft%damp_dq(itype, jtype+num_ion_types)

         ! Number of blocks with full atom2ions interactions
         num_block_full = localwork%pair_atom2ion_num_block_full_local(itype, jtype)
         iblock_offset = localwork%pair_atom2ion_full_iblock_offset(itype, jtype)
         fx_melt2ele = 0.0_wp
         fy_melt2ele = 0.0_wp
         fz_melt2ele = 0.0_wp
         do iblock = 1, num_block_full
            call update_other_block_boundaries(iblock_offset+iblock, &
                  offset_n, count_n, offset_m, count_m, &
                  istart, iend, jstart, jend)
            do i = istart, iend
               fix = 0.0_wp
               fiy = 0.0_wp
               fiz = 0.0_wp
               do j = jstart, jend
                  call minimum_image_displacement_@PBC@(a, b, c, &
                        xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                        xyz_atoms(j,1), xyz_atoms(j,2), xyz_atoms(j,3), &
                        dx, dy, dz, drnorm2)
                  if (drnorm2 < rcutsq) then
                     ! ft_ij = force_ij(drnorm2, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij)
                     include 'fumi_tosi_forceij.inc'
                     fix = fix +  dx * ft_ij
                     fiy = fiy +  dy * ft_ij
                     fiz = fiz +  dz * ft_ij

                     ! Is this meaningful?
    		     stress_tensor(1,1)=stress_tensor(1,1)-dx*dx*ft_ij
		     stress_tensor(2,2)=stress_tensor(2,2)-dy*dy*ft_ij
	     	     stress_tensor(3,3)=stress_tensor(3,3)-dz*dz*ft_ij
		     stress_tensor(1,2)=stress_tensor(1,2)-dx*dy*ft_ij
		     stress_tensor(1,3)=stress_tensor(1,3)-dx*dz*ft_ij
		     stress_tensor(2,3)=stress_tensor(2,3)-dy*dz*ft_ij
		     
                  end if
               end do
               force(i,1) = force(i,1) + fix
               force(i,2) = force(i,2) + fiy
               force(i,3) = force(i,3) + fiz
               fx_melt2ele = fx_melt2ele - fix
               fy_melt2ele = fy_melt2ele - fiy
               fz_melt2ele = fz_melt2ele - fiz
            end do
         end do
         electrodes(jtype)%force_ions(1,8) = electrodes(jtype)%force_ions(1,8) + fx_melt2ele
         electrodes(jtype)%force_ions(2,8) = electrodes(jtype)%force_ions(2,8) + fy_melt2ele
         electrodes(jtype)%force_ions(3,8) = electrodes(jtype)%force_ions(3,8) + fz_melt2ele
      end do
   end do
   call MW_timers_stop(TIMER_VAN_DER_WAALS_MELT_FORCES)   
end subroutine melt_forces_@PBC@

! ==============================================================================
! Compute Fumi-Tosi potential felt by melt ions
subroutine energy_@PBC@(localwork, ft, box, ions, xyz_ions, &
      electrodes, xyz_atoms, h)
   implicit none

   ! Parameters in
   ! -------------
   type(MW_localwork_t), intent(in) :: localwork !< Local work assignment
   type(MW_fumi_tosi_t), intent(in) :: ft      !< Fumi-Tosi potential parameters
   type(MW_box_t),   intent(in) :: box     !< Simulation box parameters

   type(MW_ion_t), intent(in) :: ions(:)        !< ions parameters
   real(wp),       intent(in) :: xyz_ions(:,:)  !< ions xyz positions

   type(MW_electrode_t), intent(in) :: electrodes(:)  !< electrode parameters
   real(wp),             intent(in) :: xyz_atoms(:,:)  !< electrode atoms xyz positions

   ! Parameters out
   ! --------------
   real(wp), intent(inout) :: h       !< Potential energy due to L-J interactions

   ! Locals
   ! ------
   integer :: num_ion_types, num_elec
   integer :: itype, jtype, i, j
   real(wp) :: a, b, c
   real(wp) :: drnorm2, rcutsq
   real(wp) :: vi
   integer :: iblock, iblock_offset, istart, iend, jstart, jend
   integer :: num_block_diag, num_block_full
   integer :: count_n, offset_n, count_m, offset_m
   real(wp) :: ft_ij, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij
   real(wp) :: drnorm, drnorm2rec, drnorm6rec, drnorm8rec
   real(wp) :: damp_dd_sum, damp_dd_term
   real(wp) :: damp_dq_sum, damp_dq_term
   integer :: k, l

   call MW_timers_start(TIMER_VAN_DER_WAALS_MELT_POTENTIAL)   

   num_ion_types = size(ions,1)
   num_elec = size(electrodes,1)

   a = box%length(1)
   b = box%length(2)
   c = box%length(3)

   rcutsq = ft%rcutsq
   h = 0.0_wp
   vi = 0.0_wp


   ! Blocks on the diagonal compute only half of the pair interactions
   do itype = 1, num_ion_types
      count_n = ions(itype)%count
      offset_n = ions(itype)%offset
      do jtype = 1, itype - 1
         count_m = ions(jtype)%count
         offset_m = ions(jtype)%offset
         eta_ij = ft%eta(itype, jtype)
         B_ij = ft%B(itype, jtype)
         C_ij = ft%C(itype, jtype)
         D_ij = ft%D(itype, jtype)
         damp_dd_ij = ft%damp_dd(itype, jtype)
         damp_dq_ij = ft%damp_dq(itype, jtype)
         ! Number of blocks with full ion2ion interactions
         num_block_full = localwork%pair_ion2ion_num_block_full_local(itype, jtype)
         iblock_offset = localwork%pair_ion2ion_full_iblock_offset(itype, jtype)

         do iblock = 1, num_block_full
            call update_other_block_boundaries(iblock+iblock_offset, &
                  offset_n, count_n, offset_m, count_m, &
                  istart, iend, jstart, jend)
            do i = istart, iend
               do j = jstart, jend
                  call minimum_image_distance_@PBC@(a, b, c, &
                        xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                        xyz_ions(j,1), xyz_ions(j,2), xyz_ions(j,3), &
                        drnorm2)
                  if (drnorm2 < rcutsq) then
                     include 'fumi_tosi_potentialij.inc'
                     vi = vi + ft_ij
                  end if
               end do
            end do
         end do
      end do

      eta_ij = ft%eta(itype, itype)
      B_ij = ft%B(itype, itype)
      C_ij = ft%C(itype, itype)
      D_ij = ft%D(itype, itype)
      damp_dd_ij = ft%damp_dd(itype, itype)
      damp_dq_ij = ft%damp_dq(itype, itype)

      num_block_diag = localwork%pair_ion2ion_num_block_diag_local(itype)
      iblock_offset = localwork%pair_ion2ion_diag_iblock_offset(itype)
      do iblock = 1, num_block_diag
         call update_diag_block_boundaries(iblock_offset+iblock, offset_n, count_n, istart, iend)
         do i = istart, iend
            do j = istart, iend
               call minimum_image_distance_@PBC@(a, b, c, &
                     xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                     xyz_ions(j,1), xyz_ions(j,2), xyz_ions(j,3), &
                     drnorm2)
               if ((drnorm2 > 0.0_wp) .and. (drnorm2 < rcutsq)) then
                  include 'fumi_tosi_potentialij.inc'
                  vi = vi + 0.5_wp * ft_ij
               end if
            end do
         end do
      end do

      ! Number of blocks with full ion2ion interactions
      num_block_full = localwork%pair_ion2ion_num_block_full_local(itype,itype)
      iblock_offset = localwork%pair_ion2ion_full_iblock_offset(itype,itype)
      do iblock = 1, num_block_full
         call update_tri_block_boundaries(iblock_offset+iblock, offset_n, count_n, &
               istart, iend, jstart, jend)
         do i = istart, iend
            do j = jstart, jend
               call minimum_image_distance_@PBC@(a, b, c, &
                     xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                     xyz_ions(j,1), xyz_ions(j,2), xyz_ions(j,3), &
                     drnorm2)
               if (drnorm2 < rcutsq) then
                  include 'fumi_tosi_potentialij.inc'
                  vi = vi + ft_ij
               end if
            end do
         end do
      end do

      do jtype = 1, num_elec
         count_m = electrodes(jtype)%count
         offset_m = electrodes(jtype)%offset

         eta_ij = ft%eta(itype, jtype+num_ion_types)
         B_ij = ft%B(itype, jtype+num_ion_types)
         C_ij = ft%C(itype, jtype+num_ion_types)
         D_ij = ft%D(itype, jtype+num_ion_types)
         damp_dd_ij = ft%damp_dd(itype, jtype+num_ion_types)
         damp_dq_ij = ft%damp_dq(itype, jtype+num_ion_types)

         ! Number of blocks with full ion2ion interactions
         num_block_full = localwork%pair_atom2ion_num_block_full_local(itype, jtype)
         iblock_offset = localwork%pair_atom2ion_full_iblock_offset(itype, jtype)

         do iblock = 1, num_block_full
            call update_other_block_boundaries(iblock+iblock_offset, &
                  offset_n, count_n, offset_m, count_m, &
                  istart, iend, jstart, jend)
            do i = istart, iend
               do j = jstart, jend
                  call minimum_image_distance_@PBC@(a, b, c, &
                        xyz_ions(i,1), xyz_ions(i,2), xyz_ions(i,3), &
                        xyz_atoms(j,1), xyz_atoms(j,2), xyz_atoms(j,3), &
                        drnorm2)
                  if (drnorm2 < rcutsq) then
                     include 'fumi_tosi_potentialij.inc'
                     vi = vi + ft_ij
                  end if
               end do
            end do
         end do
      end do
   end do

   ! Elec->Elec contribution
   do itype = 1, num_elec
      count_n = electrodes(itype)%count
      offset_n = electrodes(itype)%offset
      do jtype = 1, itype-1
         count_m =  electrodes(jtype)%count
         offset_m = electrodes(jtype)%offset

         eta_ij = ft%eta(itype+num_ion_types, jtype+num_ion_types)
         B_ij = ft%B(itype+num_ion_types, jtype+num_ion_types)
         C_ij = ft%C(itype+num_ion_types, jtype+num_ion_types)
         D_ij = ft%D(itype+num_ion_types, jtype+num_ion_types)
         damp_dd_ij = ft%damp_dd(itype+num_ion_types, jtype+num_ion_types)
         damp_dq_ij = ft%damp_dq(itype+num_ion_types, jtype+num_ion_types)
         ! Number of blocks with full interactions (below the diagonal
         num_block_full = localwork%pair_atom2atom_num_block_full_local(itype,jtype)
         iblock_offset = localwork%pair_atom2atom_full_iblock_offset(itype,jtype)

         do iblock = 1, num_block_full
            call update_other_block_boundaries(iblock_offset+iblock, &
                  offset_n, count_n, offset_m, count_m, istart, iend, jstart, jend)
            do i = istart, iend
               do j = jstart, jend
                  call minimum_image_distance_@PBC@(a, b, c, &
                        xyz_atoms(i,1), xyz_atoms(i,2), xyz_atoms(i,3), &
                        xyz_atoms(j,1), xyz_atoms(j,2), xyz_atoms(j,3), &
                        drnorm2)
                  if (drnorm2 < rcutsq)  then
                     include 'fumi_tosi_potentialij.inc'
                     vi = vi + ft_ij
                  end if
               end do
            end do
         end do
      end do

      eta_ij = ft%eta(itype+num_ion_types, itype+num_ion_types)
      B_ij = ft%B(itype+num_ion_types, itype+num_ion_types)
      C_ij = ft%C(itype+num_ion_types, itype+num_ion_types)
      D_ij = ft%D(itype+num_ion_types, itype+num_ion_types)
      damp_dd_ij = ft%damp_dd(itype+num_ion_types, itype+num_ion_types)
      damp_dq_ij = ft%damp_dq(itype+num_ion_types, itype+num_ion_types)
      num_block_diag = localwork%pair_atom2atom_num_block_diag_local(itype)
      iblock_offset = localwork%pair_atom2atom_diag_iblock_offset(itype)

      do iblock = 1, num_block_diag
         call update_diag_block_boundaries(iblock+iblock_offset, &
               offset_n, count_n, istart, iend)
         do i = istart, iend
            do j = istart, iend
               call minimum_image_distance_@PBC@(a, b, c, &
                     xyz_atoms(i,1), xyz_atoms(i,2), xyz_atoms(i,3), &
                     xyz_atoms(j,1), xyz_atoms(j,2), xyz_atoms(j,3), &
                     drnorm2)
               if ((drnorm2 > 0.0_wp) .and. (drnorm2 < rcutsq))  then
                  include 'fumi_tosi_potentialij.inc'
                  vi = vi + ft_ij
               end if
            end do
         end do
      end do

      ! Number of blocks with full interactions (below the diagonal
      num_block_full = localwork%pair_atom2atom_num_block_full_local(itype,itype)
      iblock_offset = localwork%pair_atom2atom_full_iblock_offset(itype,itype)

      do iblock = 1, num_block_full
         call update_tri_block_boundaries(iblock_offset+iblock, offset_n, count_n, &
               istart, iend, jstart, jend)
         do i = istart, iend
            do j = jstart, jend
               call minimum_image_distance_@PBC@(a, b, c, &
                     xyz_atoms(i,1), xyz_atoms(i,2), xyz_atoms(i,3), &
                     xyz_atoms(j,1), xyz_atoms(j,2), xyz_atoms(j,3), &
                     drnorm2)
               if (drnorm2 < rcutsq)  then
                  include 'fumi_tosi_potentialij.inc'
                  vi = vi + ft_ij
               end if
            end do
         end do
      end do
   end do
   h = vi

   call MW_timers_stop(TIMER_VAN_DER_WAALS_MELT_POTENTIAL)  
end subroutine energy_@PBC@

! ================================================================================
! Remove contribution from same molecule particles
!
subroutine melt_fix_molecule_forces_@PBC@(localwork, ft, molecules, box, ions, &
      xyz_ions, force,stress_tensor)
   implicit none
   ! Parameters
   ! ----------
   type(MW_localwork_t), intent(in) :: localwork
   type(MW_fumi_tosi_t), intent(in) :: ft      !< Fumi-Tosi potential parameters
   type(MW_molecule_t), intent(in) :: molecules(:) !< molecules parameters
   type(MW_box_t), intent(in) :: box !< box parameters
   type(MW_ion_t), intent(in) :: ions(:) !< ions parameters
   real(wp), intent(in) :: xyz_ions(:,:) !< melt particles coordinates
   real(wp), intent(inout) :: force(:,:) !< force on melt particles
   real(wp), intent(inout) :: stress_tensor(:,:) !< stress_tensor

   ! Locals
   ! ------
   integer :: num_molecule_types, num_sites
   integer :: mol_type, isite, jsite, imol
   integer :: iion_type, jion_type, iion, jion
   real(wp) :: a, b, c
   real(wp) :: dx, dy, dz, drnorm2
   real(wp) :: ft_ij, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij
   integer :: iion_type_offset, jion_type_offset
   real(wp) :: drnorm, drnormrec, drnorm2rec, drnorm6rec, drnorm8rec
   real(wp) :: damp_dd_sum, damp_dd_term
   real(wp) :: damp_dq_sum, damp_dq_term
   real(wp) :: scaling14
   integer :: k, l


   a = box%length(1)
   b = box%length(2)
   c = box%length(3)


   num_molecule_types = size(molecules,1)
   do mol_type = 1, num_molecule_types
      num_sites = molecules(mol_type)%num_sites

      do isite = 1, num_sites
         do jsite = 1, isite-1

            if ((molecules(mol_type)%halfinteracting_pairs(isite,jsite)).or. &
                (.not. molecules(mol_type)%interacting_pairs(isite, jsite))) then
               scaling14 = 1.0_wp
               if (molecules(mol_type)%halfinteracting_pairs(isite,jsite))scaling14 = 0.5_wp
               iion_type = molecules(mol_type)%sites(isite)
               jion_type = molecules(mol_type)%sites(jsite)
               iion_type_offset = ions(iion_type)%offset
               jion_type_offset = ions(jion_type)%offset
               eta_ij = ft%eta(iion_type, jion_type)
               B_ij = ft%B(iion_type, jion_type)
               C_ij = ft%C(iion_type, jion_type)
               D_ij = ft%D(iion_type, jion_type)
               damp_dd_ij = ft%damp_dd(iion_type, jion_type)
               damp_dq_ij = ft%damp_dq(iion_type, jion_type)
               do imol = localwork%molecules_istart(mol_type), localwork%molecules_iend(mol_type)
                  iion = imol + iion_type_offset
                  jion = imol + jion_type_offset
                  call minimum_image_displacement_@PBC@(a, b, c, &
                        xyz_ions(iion,1), xyz_ions(iion,2), xyz_ions(iion,3), &
                        xyz_ions(jion,1), xyz_ions(jion,2), xyz_ions(jion,3), &
                        dx, dy, dz, drnorm2)
                  if (drnorm2 < ft%rcutsq) then
                     ! ft_ij = force_ij(drnorm2, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij)
                     include 'fumi_tosi_forceij.inc'

                     force(iion,1) = force(iion,1) - dx * ft_ij * scaling14
                     force(iion,2) = force(iion,2) - dy * ft_ij * scaling14
                     force(iion,3) = force(iion,3) - dz * ft_ij * scaling14

                     force(jion,1) = force(jion,1) + dx * ft_ij * scaling14
                     force(jion,2) = force(jion,2) + dy * ft_ij * scaling14
                     force(jion,3) = force(jion,3) + dz * ft_ij * scaling14

		     stress_tensor(1,1)=stress_tensor(1,1)+dx*dx*ft_ij*scaling14
     		     stress_tensor(2,2)=stress_tensor(2,2)+dy*dy*ft_ij*scaling14
		     stress_tensor(3,3)=stress_tensor(3,3)+dz*dz*ft_ij*scaling14
		     stress_tensor(1,2)=stress_tensor(1,2)+dx*dy*ft_ij*scaling14
		     stress_tensor(1,3)=stress_tensor(1,3)+dx*dz*ft_ij*scaling14
		     stress_tensor(2,3)=stress_tensor(2,3)+dy*dz*ft_ij*scaling14		     

                  end if
               end do
            end if
         end do
      end do
   end do
end subroutine melt_fix_molecule_forces_@PBC@

! ================================================================================
! Remove contribution from same molecule particles
!
subroutine fix_molecule_energy_@PBC@(localwork, ft, molecules, box, ions, &
      xyz_ions, h)
   implicit none
   ! Parameters
   ! ----------
   type(MW_localwork_t), intent(in) :: localwork
   type(MW_fumi_tosi_t), intent(in) :: ft      !< Fumi-Tosi potential parameters
   type(MW_molecule_t), intent(in) :: molecules(:) !< molecules parameters
   type(MW_box_t), intent(in) :: box !< box parameters
   type(MW_ion_t), intent(in) :: ions(:) !< melt particle parameters
   real(wp), intent(in) :: xyz_ions(:,:) !< melt particles coordinates
   real(wp), intent(inout) :: h !< potential on melt particles

   ! Locals
   ! ------
   integer :: num_molecule_types, num_sites
   integer :: mol_type, isite, jsite, imol
   integer :: iion_type, jion_type, iion, jion
   real(wp) :: a, b, c
   real(wp) :: drnorm2, vi
   real(wp) :: ft_ij, eta_ij, B_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij
   real(wp) :: drnorm, drnorm2rec, drnorm6rec, drnorm8rec
   real(wp) :: damp_dd_sum, damp_dd_term
   real(wp) :: damp_dq_sum, damp_dq_term
   real(wp) :: scaling14
   integer :: k, l


   a = box%length(1)
   b = box%length(2)
   c = box%length(3)

   h = 0.0_wp
   vi = 0.0_wp

   num_molecule_types = size(molecules,1)
   do mol_type = 1, num_molecule_types
      num_sites = molecules(mol_type)%num_sites

      do isite = 1, num_sites
         do jsite = 1, isite-1

            if ((molecules(mol_type)%halfinteracting_pairs(isite,jsite)).or. &
                (.not. molecules(mol_type)%interacting_pairs(isite, jsite))) then
               scaling14 = 1.0_wp
               if (molecules(mol_type)%halfinteracting_pairs(isite,jsite))scaling14 = 0.5_wp

               iion_type = molecules(mol_type)%sites(isite)
               jion_type = molecules(mol_type)%sites(jsite)
               eta_ij = ft%eta(iion_type, jion_type)
               B_ij = ft%B(iion_type, jion_type)
               C_ij = ft%C(iion_type, jion_type)
               D_ij = ft%D(iion_type, jion_type)
               damp_dd_ij = ft%damp_dd(iion_type, jion_type)
               damp_dq_ij = ft%damp_dq(iion_type, jion_type)
               do imol = localwork%molecules_istart(mol_type), localwork%molecules_iend(mol_type)
                  iion = imol + ions(iion_type)%offset
                  jion = imol + ions(jion_type)%offset

                  call minimum_image_distance_@PBC@(a, b, c, &
                        xyz_ions(iion,1), xyz_ions(iion,2), xyz_ions(iion,3), &
                        xyz_ions(jion,1), xyz_ions(jion,2), xyz_ions(jion,3), &
                        drnorm2)
                  if (drnorm2 < ft%rcutsq) then
                     include 'fumi_tosi_potentialij.inc'
                     vi = vi + ft_ij * scaling14
                  end if
               end do
            end if
         end do
      end do
   end do
   h = - vi
end subroutine fix_molecule_energy_@PBC@
