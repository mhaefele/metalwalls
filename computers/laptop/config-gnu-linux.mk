# Compilation options
F90 := mpif90
#F90FLAGS := -O0 -g -fcheck=all
F90FLAGS := -O2 -march=native -mtune=native  -ftree-vectorize
FPPFLAGS :=  -DMW_CI 
LDFLAGS := -llapack
J := -J

# Path to pFUnit (Unit testing Framework)
PFUNIT := /usr/local
