# Compilation options 
F90 := pgfortran 
F90FLAGS := -fast -Mvect -m64 -Minfo=ccff -Mpreprocess 
F90FLAGS := -tp=pwr8 -Minfo=ccff -Mpreprocess 
F90FLAGS += -acc -ta=tesla:cuda92,cc60 -Minline 
F90FLAGS += -Minfo=accel,inline
FPPFLAGS := -DMW_SERIAL
LDFLAGS := -llapack
J := -module 

# Path to pFUnit (Unit testing Framework) 
PFUNIT := 

