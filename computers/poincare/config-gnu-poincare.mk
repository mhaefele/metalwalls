# Compilation options
F90 := mpif90
F90FLAGS := -O2  -march=native -mtune=native 
FPPFLAGS :=
LDFLAGS := -llapack
J := -J

# Path to pFUnit (Unit testing Framework)
PFUNIT := /gpfshome/mds/staff/amarin/opt/pfunit-parallel
