units               real    
dimension           3   
newton              off 
boundary            p p f 
atom_style          full 

#  Atom definition
read_data capacitor.lmp

variable CAB equal 1
variable CAC equal 2
variable CG equal 3
variable NAA equal 4

set type ${CAB} charge 0.129
set type ${CAC} charge 0.269
set type ${CG} charge 0.0
set type ${NAA} charge -0.398

# Force field
pair_style          lj/cut/coul/long 4.0
kspace_style        ewald 1.0e-8
kspace_modify       slab 6.0
pair_modify table 0

pair_coeff   ${CAB}   ${CAB}         0.09935850       3.40000000  # CAB CAB
pair_coeff   ${CAB}   ${CAC}         0.19419428       3.50000000  # CAB CAC
pair_coeff   ${CAB}   ${CG}          0.07390356       3.38500000  # CAB CG
pair_coeff   ${CAB}   ${NAA}         0.09935850       3.35000000  # CAB NAA
pair_coeff   ${CAC}   ${CAC}         0.37954900       3.60000000  # CAC CAC
pair_coeff   ${CAC}   ${CG}          0.14444310       3.48500000  # CAC CG
pair_coeff   ${CAC}   ${NAA}         0.19419428       3.45000000  # CAC NAA
pair_coeff   ${CG}    ${CG}          0.05497000       3.37000000  # CG CG
pair_coeff   ${CG}    ${NAA}         0.07390356       3.33500000  # CG NAA
pair_coeff   ${NAA}   ${NAA}         0.09935850       3.30000000  # NAA NAA

write_data ff.dat pair ij
neigh_modify check yes delay 0 every 1

# Group
group electrode type ${CG}
group mobile type ${CAB} ${CAC} ${NAA}

#  Fixes, thermostats and barostats / Run
timestep 1

dump out1 all custom 1 traj.lammpstrj id element x y z fx fy fz q
dump_modify out1 element CAB CAC CG NAA sort id format float %20.15g

dump out mobile custom 1 forces.ref id element x y z fx fy fz q
dump_modify out element CAB CAC CG NAA sort id format float %20.15g

dump out2 electrode custom 1 charges.ref id element q
dump_modify out2 element CAB CAC CG NAA sort id format float %20.15g

velocity all set 0.0 0.0 0.0

thermo_style custom step temp epair emol ke pe etotal cella cellb cellc
thermo 100

fix el all conp 1 1.9790000000 81 82 -0.50000000000 0.50000000000 inv log.conp

fix integrator mobile setforce 0.0 0.0 0.0
fix frozen electrode setforce 0.0 0.0 0.0

run 1
